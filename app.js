(function () {
    'use strict';

    angular
        .module('app', ['ui.router', 'ngMessages', 'ngStorage', 'ngMockE2E'])
        .config(config)
        .run(run)

         
        .controller(  "dealController",  ['$scope', function($scope,$state) {
            console.log("hello data");

            

            $scope.test=[
            { id:3,Cname:"dx4",Caddress:"chennai",spn:"sridhar",spe:"sri@gmail.com",spp:"4556546565",product:"MOTOE",quantity:"8"},
            {id:0,Cname:"samsung",Caddress:"chennai",spn:"sabari",spe:"sri1@gmail.com",spp:"4556546565",product:"APPLE",quantity:"7"},
            {id:1,Cname:"Del",Caddress:"erode",spn:"ram",spe:"sri2@gmail.com",spp:"4556546565",product:"LAVA",quantity:"5"},
            {id:3,Cname:"inferon",Caddress:"cbe",spn:"ravi",spe:"sri3@gmail.com",spp:"4556546565",product:"MOTOE",quantity:"6"},
            ];
             $scope.classes = [
                
                {id: 1 , class: "LAVA"},
                {id: 0 , class:"APPLE"},
                {id: 3, class:"MOTOE"},
                 {id: 5, class:"ALL"}
                
                
               
            ];

$scope.stu = '5';


if ($scope.stu == 5) {
  $scope.userList = $scope.test;
}

$scope.getClass = function(classids) {
//var stu={};
//console.log(classids.classid);
//var data =  $scope.data;

$scope.userList = [];
//console.log("users" + $scope.userList);
 
//console.log(classids.classid == 5);

if (!$scope.test) {
       return;
      }

 else if (classids.id == 5) {
  //console.log($scope.students);
  $scope.userList = $scope.test;
 return;

}
      for (var i = 0; i < $scope.test.length; i++) {
      if ($scope.test[i].id == classids.id)  {
        
       // $scope.userList = $scope.students[i];
       

       $scope.userList.push($scope.test[i]);
       //console.log(($scope.students[i]).id);
     // $scope.a=$scope.students[i];
       //var data = $scope.students[i];
       //console.log("data control" + data);
       //$scope.userList = $scope.students[i];
         continue;
      return;

        

      //return $scope.students[i]
    
       

        }
      } 

};



        
          $scope.dealer = function() {
            
              
           
          var data=  $scope.form;
          console.log(data);
         
          $scope.test.push($scope.form);
            $scope.form = {};

         
   };
   $scope.editUser = function(a) {
  console.log("edit index"+ a);
  
  console.log(a);
 
}
  $scope.delete=function(a) {
    $scope.test.splice(a,1);
  }
}]);


    function config($stateProvider, $urlRouterProvider) {
        // default route
        $urlRouterProvider.otherwise("/");

        // app routes
        $stateProvider
            .state('home', {
                url: '/',
                views:{
                  "topbar@": {
                    templateUrl: "topbar.html",
                   // controller: "topbarController"
                },
               
                 "sidebar@": {
                    templateUrl: "sidebar.html"
                    //controller: "sidebarController"
                },
                "content@": {
                    templateUrl: 'home/index.view.html',
                controller: 'Home.IndexController',
                controllerAs: 'vm'
                }     

           }
               
            })

.state('login', {
                url: '/login',
                views:{
             
           
                "content@": {
  templateUrl: 'login/index.view.html',
                controller: 'Login.IndexController',
                controllerAs: 'vm'

                }     

           }
       }
           )


           /* .state('login', {
                url: '/login',
                templateUrl: 'login/index.view.html',
                controller: 'Login.IndexController',
                controllerAs: 'vm'
            }) */
            .state('register', {
                url: '/register',
                templateUrl: 'register.html',
               // controller: 'registerController'
                
            })
            
             .state('forget', {
                url: '/forget',
                templateUrl: 'forget-password.html',
              //  controller: 'registerController'
                
            })

.state('dealer', {
                url: '/dealer',
                views:{
                    "topbar@": {
                    templateUrl: "topbar.html",
                   // controller: "topbarController"
                },
               
                 "sidebar@": {
                    templateUrl: "sidebar.html"
                    //controller: "sidebarController"
                },
           
                "content@": {
                    templateUrl: 'dealer.html',
                controller: 'dealController'

                }     

           }
       }
           )
.state('edit', {
                url: '/edit',
                views:{
                    "topbar@": {
                    templateUrl: "topbar.html",
                   // controller: "topbarController"
                },
               
                 "sidebar@": {
                    templateUrl: "sidebar.html"
                    //controller: "sidebarController"
                },
           
                "content@": {
                    templateUrl: 'edit.html',
                    controller: 'dealController'

                }     

           }
       }
           )
.state('list', {
                url: '/list',
                views:{
                    "topbar@": {
                    templateUrl: "topbar.html",
                   // controller: "topbarController"
                },
               
                 "sidebar@": {
                    templateUrl: "sidebar.html"
                    //controller: "sidebarController"
                },
           
                "content@": {
                    templateUrl: 'list.html',
                    controller: 'dealController'

                }     

           }
       }
           )

            /*.state('dealer', {
                url: '/dealer',
                templateUrl: 'dealer.html',
                controller: 'dealController'
               
            }) */
            
    }




    function run($rootScope, $http, $location, $localStorage) {
        // keep user logged in after page refresh
        if ($localStorage.currentUser) {
            $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
        }

        // redirect to login page if not logged in and trying to access a restricted page
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            var publicPages = ['/login'];
            var restrictedPage = publicPages.indexOf($location.path()) === -1;
            if (restrictedPage && !$localStorage.currentUser) {
                $location.path('/login');
            }
        });
    }
})();
